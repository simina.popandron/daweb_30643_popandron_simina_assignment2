import React from "react";
import iancu from '../commons/images/iancu.png';

class Coordonator extends React.Component {
    render(){

        let content = {
            English: {
                coord: "Coordinator",
                mat: "Teaching:",
                m1:"Computer Networks",
                m2:"Analogic and Numeric Circuits",
                m3:"Protocols and Communication Networks Project",
                m4:"Wireless Technologies and Mobile Devices"
            },
            Romana: {
                coord: "Coordonator",
                mat: "Materii predate:",
                m1:"Retele de Calculatoare",
                m2:"Circuite Analogice si Numerice",
                m3:"Proiect Protocoale si Retele de Comunicatii",
                m4:"Tehnologii wireless si dispozitive mobile"
            }
        };
        localStorage.getItem("language") === "Romana" ? (content = content.Romana) : (content = content.English);

        return(
            <div className="container-fluid page_background_color full text-center container portfolio">
                <div className="row">
                    <div className="col-md-12">
                        <div className="heading">
                            <img src="https://image.ibb.co/cbCMvA/logo.png"/>
                        </div>
                    </div>
                </div>
                <div className="bio-info" >
                    <div className="row">
                        <div className="col-md-6" >
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="bio-image">
                                        <img src={iancu} alt="iancu" width="250" height="300"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="bio-content">
                                <h3>{content.coord}</h3>
                                <h5>Bogdan Iancu</h5>
                                <h8>PhD, Senior Lecturer UTCN</h8>
                                <br/><br/>
                                <h6>{content.mat}</h6>

                                <div className="list-type1">
                                    <ol>
                                        <li><a href="#">{content.m1}</a></li>
                                        <li><a href="#">{content.m2}</a></li>
                                        <li><a href="#">{content.m3}</a></li>
                                        <li><a href="#">{content.m4}</a></li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Coordonator;