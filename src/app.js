import React, { useState } from "react";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'

import Acasa from "./acasa/acasa";
import Noutati from "./noutati/noutati";
import DespreLucrare from "./despreLucrare/despreLucrare";
import ProfilStudent from "./profilStudent/profilStudent";
import Coordonator from "./coordonator/coordonator";
import Contact from "./contact/contact";
import {Navbar} from "reactstrap";

function storeLanguageInLocalStorage(language) {
    localStorage.setItem("language", language);
}

function App(){

    let content = {
        English: {
            l:"Language: "
        },
        Romana: {
            l:"Limba: "
        }
    };
    let languageStoredInLocalStorage = localStorage.getItem("language");
    let [language, setLanguage] = useState(languageStoredInLocalStorage ? languageStoredInLocalStorage : "English");
    localStorage.getItem("language") === "Romana" ? (content = content.Romana) : (content = content.English);

        return (
            <div >
            <Router>
                <div >
                    <Navbar className="box color_navbar navbar-inverse navbar-fixed-top nav-tabs " light expand="md">
                        <div>{content.l}</div>
                    <select value={language} onChange={x=>{setLanguage(x.target.value); storeLanguageInLocalStorage(x.target.value)}}>
                        <option value="English">
                            English
                        </option>
                        <option value="Romana">
                            Romana
                        </option>
                    </select></Navbar>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Acasa/>}
                        />

                        <Route
                            exact
                            path='/noutati'
                            render={() => <Noutati/>}
                        />

                        <Route
                            exact
                            path='/despreLucrare'
                            render={() => <DespreLucrare/>}
                        />

                        <Route
                            exact
                            path='/profilStudent'
                            render={() => <ProfilStudent/>}
                        />

                        <Route
                            exact
                            path='/coordonator'
                            render={() => <Coordonator/>}
                        />

                        <Route
                            exact
                            path='/contact'
                            render={() => <Contact/>}
                        />

                        {/*Error*/}

                    </Switch>
                </div>
            </Router>
            </div>
        );
}

export default App
