import React from 'react'
import profile from './commons/images/profile.png';
import NavItem from "react-bootstrap/NavItem";

import {
    Nav,
    Navbar,
    NavLink,
} from 'reactstrap';



function NavigationBar () {
    let content = {
        English: {
            n1:"Home",
            n2:"News",
            n3:"About",
            n4:"Profile",
            n5:"Coordinator",
            n6:"Contact",
            n7:"Language"
        },
        Romana: {
            n1:"Acasa",
            n2:"Noutati",
            n3:"Despre lucrare",
            n4:"Profil student",
            n5:"Coordonator",
            n6:"Contact",
            n7:"Limba"
        }
    };
    localStorage.getItem("language") === "Romana" ? (content = content.Romana) : (content = content.English);
        return(
        <div>
            <Navbar className="color_navbar navbar-inverse navbar-fixed-top nav-tabs " light expand="md" >
                <Nav tabs >
                    <NavItem >
                        <NavLink href="/">
                            <img src={profile} className="logo"/>
                        </NavLink>
                    </NavItem>
                    <NavItem >
                        <NavLink href="/" className="text_navbar" >{content.n1} </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/noutati" className="text_navbar">{content.n2} </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/despreLucrare" className="text_navbar">{content.n3}</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/profilStudent" className="text_navbar">{content.n4}</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/coordonator" className="text_navbar">{content.n5}</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/contact" className="text_navbar">{content.n6}</NavLink>
                    </NavItem>
                    <NavItem>

                    </NavItem>
                </Nav>
            </Navbar>
        </div>
        );
}

export default NavigationBar
