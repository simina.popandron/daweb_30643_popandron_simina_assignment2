import React from "react";
import logo from '../commons/images/profile.png';
import cluj from '../commons/images/cluj.jpg';
import bus from '../commons/images/bus.jpg';
import walk from '../commons/images/walk.jpg';
import video from '../commons/images/video.mp4';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class DespreLucrare extends React.Component {
    render(){

        let content = {
            English: {
                about1:"Android application for monitoring the travel regime and acccounting the type of travel",
                about2:"The application will monitor the way the user moves (walking / public transport / car), and depending on the means used, points will be awarded. As with any game, the number of points results in levels, and at each level the player will be rewarded.",
                ob:"Objectives",
                o1:"➸ Encourage the people of Cluj to use the public transport, to the detriment of their personal cars or the available taxi / car shareing services.",
                o2:"➸ Motivate users to walk as much as possible, minimizing the use of any means of transport by awarding points that result in physical rewards.",
                o3:"➸ Air purification due to the decrease in the number of cars in traffic.",
                o4:"➸ Making a useful application for both the user and those around him."
            },
            Romana: {
                about1:"Aplicatie Android pentru monitorizarea regimului de deplasare si contorizarea tipului de deplasare",
                about2:"Aplicatia va monitoriza modul de deplasare a utilizatorului(mers pe jos/transport in comun/masina), iar in functie de mijlocul folosit se vor acorda puncte. Similar oricarui joc, numarul de puncte rezulta in nivele, iar la fiecare nivel utilizarotul va fi recompensat.",
                ob:"Obiective",
                o1:"➸ Indemnarea clujenilor de a folosi transportul in comun, in detrimentul masinilor personale sau a serviciilor de taxi/ car share-ing disponibile.",
                o2:"➸ Motivarea utilizatorilor de a merge pe jos cat mai mult, minimizand utilizarea oricarui mijloc de transport prin acordarea unor puncte care rezulta in premii fizice.",
                o3:"➸ Purificarea aerului datorita scaderii numarului de masini din trafic.",
                o4:"➸ Realizarea unei aplicatii folositoare atat pentru utilizator cat si pentru cei din jurul sau."
            }
        };
        localStorage.getItem("language") === "Romana" ? (content = content.Romana) : (content = content.English);

        return(

            <div className="container-fluid page_background_color full">

                <div className="container text-center">
                    <div className="row" >
                        <div className="col-sm-5 col-md-6" >

                            <Carousel showThumbs={false} dynamicHeight>
                                <div>
                                    <img src={logo} />
                                </div>
                                <div>
                                    <img src={cluj} />
                                </div>
                                <div>
                                    <img src={bus} />
                                </div>
                                <div>
                                    <img src={walk} />
                                </div>
                                <div>
                                    <video width="500" height="500" controls >
                                        <source src={video} type="video/mp4"/>
                                    </video>
                                </div>
                            </Carousel>
                        </div>

                        <div className="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-0">
                            <h4 className={"text_mare_about"}>{content.about1}</h4>
                            <br/>
                            <h9 className={"text_mic_about"}>{content.about2}</h9>
                        </div>
                    </div>
                    <br/>
                    <br/>

                    <h3 className={"text_mare_about"}>{content.ob}</h3>
                    <div className="row text_mic_about">
                        <div className="col-sm-3">
                            <p>{content.o1}</p>
                        </div>
                        <div className="col-sm-3">
                            <p>{content.o2}</p>
                        </div>
                        <div className="col-sm-3">
                                <p>{content.o3}</p>
                        </div>
                        <div className="col-sm-3">
                            <p>{content.o4}</p>
                        </div>
                    </div>
                </div>




            </div>



        );
    }
}
export default DespreLucrare;