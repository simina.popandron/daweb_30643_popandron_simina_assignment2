import React from 'react'

let xml=null;
function Noutati() {
    let content = {
        English: {
            f:"News.xml"
        },
        Romana: {
            f:"Noutati.xml"
        }
    };
    localStorage.getItem("language") === "Romana" ? (content = content.Romana) : (content = content.English);
    function f1(){
        const src = content.f;
        fetch(src)
            .then((response) => response.text())
            .then((responseText) => {
                xml=responseText.toString();
                console.log(responseText);
                console.log(xml);
                return responseText; })
            .catch((error) => {console.log('Error fetching the feed: ', error);});
        return xml;
    }
/////////////////////In console log se vad bine textele in functie de limba aleasa, insa pe pagina apar cu un pas in urma fata de schimbarea limbii ///////////////////////
    return (
        <div className="container-fluid alb full">
            <div className="holder">
                <h3><span className="tbl"  dangerouslySetInnerHTML={{__html: f1()}}/></h3>
            </div>
        </div>
    )
}
export default Noutati;